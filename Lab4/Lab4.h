// Lab3.h : Declarations for Lab3.cpp
// Han Wang; You Chen
// wang.han@wustl.edu; yoyo.chen@wustl.edu
// File summary: usage message function
//				key enum values defined

#pragma once

#include <memory>
#include "GameBase.h"

using namespace std;

int usage(char *program_name, char *info);

enum lab4 {
	program_name = 0,

	bad_allocation
};