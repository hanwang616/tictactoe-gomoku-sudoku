// GameBase.cpp 
// Han Wang; You Chen
// wang.han@wustl.edu; yoyo.chen@wustl.edu
// File summary:  function definitions for the base class
//				can promt the user to input game commands
//				can continuously opertating the name 
//				can check the argument passed to the program and construct different games

#include "stdafx.h"
#include "GameBase.h"

shared_ptr<GameBase> GameBase::smartPtr = nullptr;


// obtain a shared smart pointer to the object through which to invoke methods on the object
shared_ptr<GameBase> GameBase::instance() {
	if (GameBase::smartPtr == nullptr) {
		throw null_smart_pointer;
	}
	else {
		return GameBase::smartPtr;
	}
}

// prompt the user to input coordinates or other commands
int GameBase::prompt(unsigned &x, unsigned &y) {
	while (true) {
		cout << "Please enter a coordinate 'x,y' or 'quit' to end the game" << endl;
		string input;
		cin >> input;
		if (input == "quit") {
			//end game
			return quit;
		}
		else {
			size_t pos = input.find(",");
			if (pos != string::npos) {
				input.replace(pos, 1, " ");
				istringstream iss = istringstream(input);
				if (iss >> x && iss >> y) {
					return success;
				}
			}
		}
	}
}

// prompt the user to input coordinates and a number for Sudoku game
int GameBase::prompt(unsigned &x, unsigned &y, unsigned& number) {
	while (true) {
		cout << "Please enter a coordinate and number 'x,y,number' or 'quit' to end the game" << endl;
		string input;
		cin >> input;
		if (input == "quit") {
			//end game
			return quit;
		}
		else {
			size_t pos = input.find(",");
			if (pos != string::npos) {
				input.replace(pos, 1, " ");
				pos = input.find(",");
				if (pos != string::npos) {
					input.replace(pos, 1, " ");
				}
				istringstream iss = istringstream(input);
				if (iss >> x && iss >> y && iss >> number) {
					return success;
				}
			}
		}
	}
}

// operate the game and show the game result
int GameBase::play()
{
	print();
	while (true) {
		if (turn() == quit) {
			cout << ((board_width - 2) * (board_height - 2) - remainingTurns) << " turns played, user quited!" << endl;
			saveGame();
			return quit;
		}
		if (done()) {
			cout << whoseTurn << " has won!" << endl;
			overWriteGameFile();
			return success;
		}
		if (stalement()) {
			cout << ((board_width - 2) * (board_height - 2) - remainingTurns) << " turns played, no winning moves remain!" << endl;
			overWriteGameFile();
			return tie;
		}
	}
}

// check the arguments passed to the program and initialize games according to the arguments
void GameBase::checkArgs(int argc, char * argv[])
{
	if (GameBase::smartPtr == nullptr) {
		if (argc == argc_ttt && strcmp(argv[game_name_input], "TicTacToe") == 0) {
			GameBase::smartPtr = make_shared<TicTacToeGame>(*new TicTacToeGame());
		}
		else if (argc == argc_gmk1 && strcmp(argv[game_name_input], "Gomoku") == 0) {
			GameBase::smartPtr = make_shared<GomokuGame>(*new GomokuGame());
		}
		else if (argc == argc_gmk2 && strcmp(argv[game_name_input], "Gomoku") == 0) {
			istringstream issSL(argv[side_length_input]);
			unsigned sideLength;
			if (issSL >> sideLength && sideLength >= 1) {
				GameBase::smartPtr = make_shared<GomokuGame>(*new GomokuGame(sideLength));
			}
		}
		else if (argc == argc_gmk3 && strcmp(argv[game_name_input], "Gomoku") == 0) {
			istringstream issSL(argv[side_length_input]);
			istringstream issG(argv[goal_input]);
			unsigned sideLength;
			unsigned goal;
			if (issSL >> sideLength && issG >> goal && sideLength >= 1 && goal >= 3) {
				GameBase::smartPtr = make_shared<GomokuGame>(*new GomokuGame(sideLength, goal));
			}
		}
		else if (argc == argc_sdk && strcmp(argv[game_name_input], "Sudoku") == 0) {
			GameBase::smartPtr = make_shared<SudokuGame>(*new SudokuGame());
		}
		else {
			throw bad_game_name;
		}
	}
	else {
		throw smart_pointer_not_null;
	}
}
