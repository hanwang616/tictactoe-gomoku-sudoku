// GamePiece.h : Declarations for GamePiece.cpp
// Han Wang; You Chen
// wang.han@wustl.edu; yoyo.chen@wustl.edu
// File summary: Specification of a game_piece, containing color, name and display name.

#ifndef GAMEPIECE_H
#define GAMEPIECE_H

#include <iostream>
#include <string>

using namespace std;

enum piece_color {red, black, white, border, no_color, invalid};

struct game_piece {
	game_piece(piece_color c, string n, string d);
	piece_color color;
	string name;
	string display;
};

#endif /* GAMEPIECE_H */