// SudokuGame.cpp 
// Han Wang; You Chen
// wang.han@wustl.edu; yoyo.chen@wustl.edu
// File summary:  function definitions for derived class SudokuGame
//				Has a constructor which initializes the game board
//				Defines an operator for printing out the game board
//				can decide on whether the game is done
//				can decide on whether the game reachs stalement by checking remaining moves
//				can alternate turns between two players


#include "stdafx.h"
#include "GameBase.h"

// defines an operator for printing out the current state of the game board
ostream & operator<< (ostream & o, const SudokuGame &game) {
	for (int i = game.board_height - 1; i >= -1; --i) {
		if (i == -1) {
			o << "#||0|1|2||3|4|5||6|7|8||" << endl;
		}
		else {
			o << i << "||";
			for (unsigned j = 0; j < game.board_width; ++j) {
				if (game.gameBoard[game.board_width * i + j].display.substr(0, 1) == "0") {
					o << " |";
				}
				else {
					o << game.gameBoard[game.board_width * i + j].display.substr(0, 1) << "|";
				}
				if (j % 3 == 2) {
					o << "|";
				}
			}
			o << endl;
			if (i % 3 == 0) {
				o << "=||=|=|=||=|=|=||=|=|=||" << endl;
			}
			else {
				o << "-||-|-|-||-|-|-||-|-|-||" << endl;
			}
		}
	}
	return o;
}

// Check whether the file contains a valid previous saved game version
//  If so, initialize the game according to the file
//  If not, initialize the game as it is firstly started up
bool SudokuGame::readFromFile() {
	ifstream ifs;
	ifs.open("Sudoku.txt");
	if (ifs.is_open()) {
		string s;

		//game name
		if (getline(ifs, s)) {
			if (s == "Sudoku") {

				//reading game pieces
				if (getline(ifs, s)) {
					if (s == "NO DATA") {
						return false;
					}
					istringstream issPcs(s);
					for (unsigned i = 0; i < board_width*board_height; i++) {
						string p;
						if (issPcs >> p) {
							if (p == "BLANK") {
								gameBoard.push_back(game_piece(no_color, "", " "));
							}
							else {
								gameBoard.push_back(game_piece(no_color, "", p));
							}
						}
						else {
							return false;
						}
					}
					return true;
				}
			}
		}
	}
	return false;
}

// initialize the Sudoku game
SudokuGame::SudokuGame()
{
	board_width = default_sdk_board_width;
	board_height = default_sdk_board_height;
	whoseTurn = "X";
	remainingTurns = 81;
	longestDisplayStringLength = 1;

	if (!readFromFile()) {
		ifstream ifs;
		ifs.open("sudoku0.txt");
		if (ifs.is_open()) {
			for (unsigned i = 0; i < board_height; i++) {
				string s;
				if (getline(ifs, s)) {
					istringstream issPcs(s);
					string p;
					for (unsigned j = 0; j < board_width; ++j) {
						if (issPcs >> p) {
							if (p == "0") {
								gameBoard.push_back(game_piece(no_color, "", " "));
							}
							else {
								gameBoard.push_back(game_piece(no_color, "", p + "O"));
							}
						}
					}
				}
			}
		}
	}
}

// printing out the game board
void SudokuGame::print() {
	cout << *this;
}


// decide on whether the someone wins the game
bool SudokuGame::done()
{
	if (emptySquaresRemain()) {
		return false;
	}

	// check same row
	for (unsigned row = 0; row < 9; ++row) {
		for (unsigned col1 = 0; col1 < 9; ++col1) {
			for (unsigned col2 = col1 + 1; col2 < 9; ++col2) {
				if (gameBoard[row * 9 + col1].display.substr(0, 1) == gameBoard[row * 9 + col2].display.substr(0, 1)) {
					return false;
				}
			}
		}
	}

	// check same column
	for (unsigned col = 0; col < 9; ++col) {
		for (unsigned row1 = 0; row1 < 9; ++row1) {
			for (unsigned row2 = row1 + 1; row2 < 9; ++row2) {
				if (gameBoard[row1 * 9 + col].display.substr(0, 1) == gameBoard[row2 * 9 + col].display.substr(0, 1)) {
					return false;
				}
			}
		}
	}

	// check same region
	unsigned reg1[] = { 0,1,2,9,10,11,18,19,20 };
	unsigned reg2[] = { 3,4,5,12,13,14,21,22,23 };
	unsigned reg3[] = { 6,7,8,15,16,17,24,25,26 };
	unsigned reg4[] = { 27,28,29,36,37,38,45,46,47 };
	unsigned reg5[] = { 30,31,32,39,40,41,48,49,50 };
	unsigned reg6[] = { 33,34,35,42,43,44,51,52,53 };
	unsigned reg7[] = { 54,55,56,63,64,65,72,73,74 };
	unsigned reg8[] = { 57,58,59,66,67,68,75,76,77 };
	unsigned reg9[] = { 60,61,62,69,70,71,78,79,80 };
	// region1
	for (unsigned i = 0; i < 9; ++i) {
		for (unsigned j = i + 1; j < 9; ++j) {
			if (gameBoard[reg1[i]].display == gameBoard[reg1[j]].display) {
				return false;
			}
		}
	}
	// region2
	for (unsigned i = 0; i < 9; ++i) {
		for (unsigned j = i + 1; j < 9; ++j) {
			if (gameBoard[reg2[i]].display == gameBoard[reg2[j]].display) {
				return false;
			}
		}
	}
	// region3
	for (unsigned i = 0; i < 9; ++i) {
		for (unsigned j = i + 1; j < 9; ++j) {
			if (gameBoard[reg3[i]].display == gameBoard[reg3[j]].display) {
				return false;
			}
		}
	}
	// region4
	for (unsigned i = 0; i < 9; ++i) {
		for (unsigned j = i + 1; j < 9; ++j) {
			if (gameBoard[reg4[i]].display == gameBoard[reg4[j]].display) {
				return false;
			}
		}
	}
	// region5
	for (unsigned i = 0; i < 9; ++i) {
		for (unsigned j = i + 1; j < 9; ++j) {
			if (gameBoard[reg5[i]].display == gameBoard[reg5[j]].display) {
				return false;
			}
		}
	}
	// region6
	for (unsigned i = 0; i < 9; ++i) {
		for (unsigned j = i + 1; j < 9; ++j) {
			if (gameBoard[reg6[i]].display == gameBoard[reg6[j]].display) {
				return false;
			}
		}
	}
	// region7
	for (unsigned i = 0; i < 9; ++i) {
		for (unsigned j = i + 1; j < 9; ++j) {
			if (gameBoard[reg7[i]].display == gameBoard[reg7[j]].display) {
				return false;
			}
		}
	}
	// region8
	for (unsigned i = 0; i < 9; ++i) {
		for (unsigned j = i + 1; j < 9; ++j) {
			if (gameBoard[reg8[i]].display == gameBoard[reg8[j]].display) {
				return false;
			}
		}
	}
	// region9
	for (unsigned i = 0; i < 9; ++i) {
		for (unsigned j = i + 1; j < 9; ++j) {
			if (gameBoard[reg9[i]].display == gameBoard[reg9[j]].display) {
				return false;
			}
		}
	}

	return true;
}


// placeholder for pure virtual function, no meaning for Sudoku
bool SudokuGame::stalement()
{
	return false;
}

// check whether there are still empty squares
bool SudokuGame::emptySquaresRemain() {
	for (game_piece gp : gameBoard) {
		if (gp.display == " ") {
			return true;
		}
	}
	return false;
}


// just turn
int SudokuGame::turn()
{
	while (true) {
		unsigned x;
		unsigned y;
		unsigned number;
		int msg = prompt(x, y, number);
		if (msg == quit) {
			return quit;
		}
		else if (msg == success) {
			if (x >= 0 && x < board_width && y >= 0 && y < board_height && gameBoard[x + y * board_width].display.length() == 1 && number < 10 && number > 0) {
				// setting the new move
				gameBoard[x + y * board_width].color = black;
				gameBoard[x + y * board_width].name = "move";
				gameBoard[x + y * board_width].display = static_cast<ostringstream*>(&(ostringstream() << number))->str();

				longestDisplayStringLength = max(gameBoard[x + y * board_width].display.length(), longestDisplayStringLength);

				cout << *this << endl;

				return success;
			}
			else {
				continue;
			}
		}
	}
}

// Check whether the player wants to save the game
//  If so, save the current state of the game into a file
//  If not, overwrites the game file with information indicating that
//    next time the game should be start at the beginning
void SudokuGame::saveGame() {
	while (true) {
		cout << "Do you want to save current game? [yes/no]" << endl;
		string input;
		cin >> input;
		if (input == "yes") {
			ofstream ofs;
			ofs.open("Sudoku.txt");
			if (ofs.is_open()) {
				ofs << "Sudoku" << endl;

				for (unsigned i = 0; i < SudokuGame::board_height; ++i) {
					for (unsigned j = 0; j < SudokuGame::board_width; ++j) {
						if (SudokuGame::gameBoard[SudokuGame::board_width * i + j].display == " ") {
							ofs << "BLANK" << " ";
						}
						else {
							ofs << SudokuGame::gameBoard[SudokuGame::board_width * i + j].display << " ";
						}
					}
				}
				ofs << endl;

				ofs.close();
			}
			return;
		}
		else if (input == "no") {
			overWriteGameFile();
			return;
		}
	}
}


// Overwrite the file with information indicating that next time the game should be start at the beginning
void SudokuGame::overWriteGameFile() {
	ofstream ofs;
	ofs.open("Sudoku.txt");
	if (ofs.is_open()) {
		ofs << "Sudoku" << endl;
		ofs << "NO DATA" << endl;
		ofs.close();
	}
}


// Specify a Sudoku's method which operates the game and shows the game result
int SudokuGame::play()
{
	print();
	while (true) {
		if (turn() == quit) {
			cout << "User quited!" << endl;
			saveGame();
			return quit;
		}
		if (done()) {
			cout << " You win!" << endl;
			overWriteGameFile();
			return success;
		}
		if (!emptySquaresRemain()) {
			cout << "No empty squares! Change some tiles!" << endl;
		}
	}
}