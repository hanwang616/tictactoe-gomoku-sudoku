// GamePiece.h : Declarations for GameBase.cpp, TicTacToeGame.cpp, GomokuGame.cpp
// Han Wang; You Chen
// wang.han@wustl.edu; yoyo.chen@wustl.edu
// File summary: class declarations for GameBase, TicTacToeGame, GomokuGame
//				ostream insertion operators declaration
//				key enum values defined for GameBase, TicTacToeGame, GomokuGame
//				key enum values defined for game outcomes

#pragma once

#include "GamePiece.h"

#include <sstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <memory>
#include <fstream>

using namespace std;

class GameBase {
public:
	static shared_ptr<GameBase> instance();

	static void checkArgs(int argc, char* argv[]);
	virtual int play();

protected:
	static shared_ptr<GameBase> smartPtr;
	void virtual saveGame() = 0;
	void virtual overWriteGameFile() = 0;
	bool virtual readFromFile() = 0;

	vector<game_piece> gameBoard;
	unsigned board_width;
	unsigned board_height;
	string whoseTurn;
	unsigned remainingTurns;
	vector<unsigned> moves1;
	vector<unsigned> moves2;
	size_t longestDisplayStringLength;

	virtual void print() = 0;
	virtual bool done() = 0;
	virtual bool stalement() = 0;
	virtual int turn() = 0;

	virtual int prompt(unsigned &w, unsigned &h);
	virtual int prompt(unsigned &w, unsigned &h, unsigned& number);
};

class TicTacToeGame : public GameBase {
	friend ostream & operator<< (ostream &o, const TicTacToeGame &game);
public:
	TicTacToeGame();
protected:
	virtual void saveGame();
	virtual void overWriteGameFile();
	bool virtual readFromFile();

	virtual void print();
	virtual bool done();
	virtual bool stalement();
	virtual int turn();
private:
	bool movesRemain();
};

ostream & operator<< (ostream &o, const TicTacToeGame &game);

class GomokuGame : public GameBase {
	friend ostream & operator<< (ostream &o, const GomokuGame &game);
public:
	GomokuGame();
	GomokuGame(unsigned sideLengthIn);
	GomokuGame(unsigned sideLengthIn, unsigned goalIn);
protected:
	virtual void saveGame();
	virtual void overWriteGameFile();
	bool virtual readFromFile();

	unsigned goal = 5;
	virtual void print();
	virtual bool done();
	virtual bool stalement();
	virtual int turn();
private:
	bool movesRemain();
};

ostream & operator<< (ostream &o, const GomokuGame &game);

class SudokuGame : public GameBase {
	friend ostream & operator<< (ostream &o, const SudokuGame &game);
public:
	SudokuGame();
	virtual int play();
protected:
	virtual void saveGame();
	virtual void overWriteGameFile();
	bool virtual readFromFile();

	virtual void print();
	virtual bool done();
	virtual bool stalement();
	virtual int turn();
private:
	bool emptySquaresRemain();
};

enum exit_conditions {
	success,
	wrong_format_of_commandline_arguments,
	quit,
	tie
};

enum game_base {
	argc_ttt = 2,
	argc_gmk1 = 2,
	argc_gmk2 = 3,
	argc_gmk3 = 4,
	argc_sdk = 2,

	game_name_input = 1,
	side_length_input = 2,
	goal_input = 3
};
enum tic_tac_toe_game {
	default_ttt_board_width = 5,
	default_ttt_board_height = 5
};
enum gomoku_game {
	default_gmk_board_width = 21,
	default_gmk_board_height = 21,
	default_gmk_longestDisplayStringLength = 2
};
enum sudoku_game {
	default_sdk_board_width = 9,
	default_sdk_board_height = 9
};
enum exceptions {
	null_smart_pointer,
	smart_pointer_not_null,
	bad_game_name
};