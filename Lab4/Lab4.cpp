// Lab4.cpp : Defines the entry point for the console application.
// Han Wang; You Chen
// wang.han@wustl.edu; yoyo.chen@wustl.edu
// File summary: reads in one, two or three arguments, deciding on which game to start 
//				returns a usage message when the passing in incorrect commands


#include "stdafx.h"
#include "Lab4.h"

int main(int argc, char* argv[])
{
	try {
		GameBase::checkArgs(argc, argv);

		try {
			return GameBase::instance()->play();
		}
		catch (exceptions e) {
			if (e == null_smart_pointer) {
				return bad_allocation;
			}
		}
		
	}
	catch (exceptions e) {
		if (e == bad_game_name) {
			return usage(argv[program_name], 
				"[TicTacToe]\n\t to play Tic Tac Toe or\n[Gomoku]\n\t to play 19x19 Gomoku or\n[Gomoku] [Board Dimension]\n\t to specify board dimension or\n[Gomoku] [Board Dimension] [Number Of Pieces To Win]\n\t to also specify number of connecting pieces to win or \n[Sudoku]\n\t to play Sudoku");
		}
		return bad_allocation;
	}
}


// 
int usage(char *program_name, char *info) {
	cout << "Usage: " << program_name << endl << info << endl;
	return wrong_format_of_commandline_arguments;
}