// GomokuGame.cpp 
// Han Wang; You Chen
// wang.han@wustl.edu; yoyo.chen@wustl.edu
// File summary:  function definitions for derived class GomokuGame
//				Has two constructors which initializes the game board for different games
//				Defines an operator for printing out the game board
//				can decide on whether the game is done
//				can decide on whether the game reachs stalement by checking remaining moves
//				can alternate turns between two players
//				can save the game state

#include "stdafx.h"
#include "GameBase.h"

// defines an operator for printing out the current state of the game board
ostream & operator<< (ostream & o, const GomokuGame &game) {
	for (int i = game.board_height - 1; i >= 0; --i) {
		if (i == 0 || i == game.board_height - 1) {
			o << " " << setw(game.longestDisplayStringLength + 1);
		}
		else {
			o << i << setw(game.longestDisplayStringLength + 1);
		}
		for (int j = 0; j < static_cast<int>(game.board_width); ++j) {
			o << game.gameBoard[game.board_width * i + j].display << setw(game.longestDisplayStringLength + 1);
		}
		o << endl;
	}
	o << " " << setw(game.longestDisplayStringLength + 1);
	for (int i = 0; i < static_cast<int>(game.board_width); ++i) {
		if (i == 0 || i == game.board_height - 1) {
			o << " " << setw(game.longestDisplayStringLength + 1);
		}
		else {
			o << i << setw(game.longestDisplayStringLength + 1);
		}
	}
	o << endl;

	return o;
}


// Check whether the file contains a valid previous saved game version
//  If so, initialize the game according to the file
//  If not, initialize the game as it is firstly started up
bool GomokuGame::readFromFile() {
	ifstream ifs;
	ifs.open("Gomoku.txt");
	if (ifs.is_open()) {
		string s;

		//game name
		if (getline(ifs, s)) {
			if (s == "Gomoku") {

				//gameboard dimensions
				if (getline(ifs, s)) {
					if (s == "NO DATA") {
						return false;
					}
					istringstream issDim(s);
					int dimension = -1;
					if (issDim >> dimension && dimension > 0) {
						board_width = dimension;
						board_height = dimension;

						//goal
						if (getline(ifs, s)) {
							istringstream issGoal(s);
							int g = -1;
							if (issGoal >> g && g > 0) {
								goal = g;

								//whoseTurn
								if (getline(ifs, s) && (s == "B" || s == "W")) {
									whoseTurn = s;
											
									//remainingTurns
									if (getline(ifs, s)) {
										istringstream issRT(s);
										int rt = -1;
										if (issRT >> rt && rt > 0) {
											remainingTurns = rt;

											//moves1
											if (getline(ifs, s)) {
												istringstream issM1(s);
												unsigned e1;
												while (issM1 >> e1) {
													GomokuGame::moves1.push_back(e1);
												}

												//moves2
												if (getline(ifs, s)) {
													istringstream issM2(s);
													unsigned e2;
													while (issM2 >> e2) {
														GomokuGame::moves2.push_back(e2);
													}

													//longestDisplayStringLength
													if (getline(ifs, s)) {
														istringstream issL(s);
														size_t l;
														if (issL >> l) {
															longestDisplayStringLength = l;
														
															//reading game pieces
															if (getline(ifs, s)) {
																istringstream issPcs(s);
																for (unsigned i = 0; i < board_width*board_height; i++) {
																	if (i < board_width || i > board_width*(board_height - 1) - 1 || i % board_width == 0 || i % board_width == board_width - 1) {
																		gameBoard.push_back(game_piece(border, "", " "));
																	}
																	else {
																		string p;
																		if (issPcs >> p) {
																			if (p == "BLANK") {
																				gameBoard.push_back(game_piece(no_color, "", " "));
																			}
																			else {
																				gameBoard.push_back(game_piece(no_color, "", p));
																			}
																		}
																		else {
																			return false;
																		}
																	}
																}
																return true;
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	return false;
}

// initialize the Gomoku game
GomokuGame::GomokuGame()
{
	if (!readFromFile()) {
		board_width = default_gmk_board_width;
		board_height = default_gmk_board_height;
		whoseTurn = "W";
		remainingTurns = (board_width - 2) * (board_height - 2);
		longestDisplayStringLength = default_gmk_longestDisplayStringLength;
		for (unsigned i = 0; i < board_width*board_height; i++) {
			if (i < board_width || i > board_width*(board_height - 1) - 1 || i % board_width == 0 || i % board_width == board_width - 1) {
				gameBoard.push_back(game_piece(border, "", " "));
			}
			else {
				gameBoard.push_back(game_piece(no_color, "", " "));
			}
		}
	}
}

// initialize the Gomoku game while letting the user decide the size of the board
GomokuGame::GomokuGame(unsigned sideLengthIn)
{
	if (!readFromFile()) {
		board_width = sideLengthIn + 2;
		board_height = sideLengthIn + 2;
		whoseTurn = "W";
		remainingTurns = (board_width - 2) * (board_height - 2);
		longestDisplayStringLength = static_cast<size_t>(log10(sideLengthIn)) + 1;
		for (unsigned i = 0; i < board_width*board_height; i++) {
			if (i < board_width || i > board_width*(board_height - 1) - 1 || i % board_width == 0 || i % board_width == board_width - 1) {
				gameBoard.push_back(game_piece(border, "", " "));
			}
			else {
				gameBoard.push_back(game_piece(no_color, "", " "));
			}
		}
	}
}

// initialize the Gomoku game while letting the user decide the size of game board 
//								and the number of connecting pieces needed to win  
GomokuGame::GomokuGame(unsigned sideLengthIn, unsigned goalIn)
{
	if (!readFromFile()) {
		goal = goalIn;
		board_width = sideLengthIn + 2;
		board_height = sideLengthIn + 2;
		whoseTurn = "W";
		remainingTurns = (board_width - 2) * (board_height - 2);
		longestDisplayStringLength = static_cast<size_t>(log10(sideLengthIn)) + 1;
		for (unsigned i = 0; i < board_width*board_height; i++) {
			if (i < board_width || i > board_width*(board_height - 1) - 1 || i % board_width == 0 || i % board_width == board_width - 1) {
				gameBoard.push_back(game_piece(border, "", " "));
			}
			else {
				gameBoard.push_back(game_piece(no_color, "", " "));
			}
		}
	}
}

// printing out the game board
void GomokuGame::print() {
	cout << *this;
}

// decide on whether someone wins the game
bool GomokuGame::done()
{
	// check left to right
	for (unsigned i = 1; i < board_width - goal; ++i) {
		for (unsigned j = 1; j < board_height - 1; ++j) {
			bool found = true;
			for (unsigned c = 1; c < goal; ++c) {
				if (gameBoard[i + j * board_width].display == " " || gameBoard[i + j * board_width].display != gameBoard[i + j * board_width + c].display) {
					found = false;
				}
			}
			if (found) {
				return true;
			}
		}
	}
	
	// check bottom to top
	for (unsigned i = 1; i < board_width - 1; i++) {
		for (unsigned j = 1; j < board_height - goal; ++j) {
			bool found = true;
			for (unsigned c = 1; c < goal; ++c) {
				if (gameBoard[i + j * board_width].display == " " || gameBoard[i + j * board_width].display != gameBoard[i + j * board_width + c * board_height].display) {
					found = false;
				}
			}
			if (found) {
				return true;
			}
		}
	}

	// check bottom-left to top-right
	for (unsigned i = 1; i < board_width - goal; ++i) {
		for (unsigned j = 1; j < board_height - goal; ++j) {
			bool found = true;
			for (unsigned c = 1; c < goal; ++c) {
				if (gameBoard[i + j * board_width].display == " " || gameBoard[i + j * board_width].display != gameBoard[i + j * board_width + c * board_height + c].display) {
					found = false;
				}
			}
			if (found) {
				return true;
			}
		}
	}

	// check top-left to bottom-right
	for (unsigned i = 1; i < board_width - goal; ++i) {
		for (unsigned j = goal; j < board_height - 1; ++j) {
			bool found = true;
			for (unsigned c = 1; c < goal; ++c) {
				if (gameBoard[i + j * board_width].display == " " || gameBoard[i + j * board_width].display != gameBoard[i + j * board_width - c * board_height + c].display) {
					found = false;
				}
			}
			if (found) {
				return true;
			}
		}
	}

	return false;
}

// decide on whether the game reachs a stalement situation
bool GomokuGame::stalement()
{
	if (done() || movesRemain()) {
		return false;
	}
	else {
		return true;
	}
}

// check whether there are still remaining steps which possibly leads to winning
bool GomokuGame::movesRemain() {
	// check left to right
	for (unsigned i = 1; i < board_width - goal; ++i) {
		for (unsigned j = 1; j < board_height - 1; ++j) {
			bool found = true;
			string displayTemp = " ";
			for (unsigned c = 0; c < goal; ++c) {
				if (displayTemp == " " && gameBoard[i + j * board_width + c].display != " ") {
					displayTemp = gameBoard[i + j * board_width + c].display;
				}
				else if (displayTemp != " " && gameBoard[i + j * board_width + c].display != " " && gameBoard[i + j * board_width + c].display != displayTemp) {
					found = false;
				}
			}
			if (found) {
				return true;
			}
		}
	}

	// check bottom to top
	for (unsigned i = 1; i < board_width - 1; i++) {
		for (unsigned j = 1; j < board_height - goal; ++j) {
			bool found = true;
			string displayTemp = " ";
			for (unsigned c = 0; c < goal; ++c) {
				if (displayTemp == " " && gameBoard[i + j * board_width + c * board_height].display != " ") {
					displayTemp = gameBoard[i + j * board_width + c * board_height].display;
				}
				else if (displayTemp != " " && gameBoard[i + j * board_width + c * board_height].display != " " && gameBoard[i + j * board_width + c * board_height].display != displayTemp) {
					found = false;
				}
			}
			if (found) {
				return true;
			}
		}
	}

	// check bottom-left to top-right
	for (unsigned i = 1; i < board_width - goal; ++i) {
		for (unsigned j = 1; j < board_height - goal; ++j) {
			bool found = true;
			string displayTemp = " ";
			for (unsigned c = 0; c < goal; ++c) {
				if (displayTemp == " " && gameBoard[i + j * board_width + c * board_height + c].display != " ") {
					displayTemp = gameBoard[i + j * board_width + c * board_height + c].display;
				}
				else if (displayTemp != " " && gameBoard[i + j * board_width + c * board_height + c].display != " " && gameBoard[i + j * board_width + c * board_height + c].display != displayTemp) {
					found = false;
				}
			}
			if (found) {
				return true;
			}
		}
	}

	// check top-left to bottom-right
	for (unsigned i = 1; i < board_width - goal; ++i) {
		for (unsigned j = goal; j < board_height - 1; ++j) {
			bool found = true;
			string displayTemp = " ";
			for (unsigned c = 0; c < goal; ++c) {
				if (displayTemp == " " && gameBoard[i + j * board_width - c * board_height + c].display != " " && gameBoard[i + j * board_width - c * board_height + c].display != displayTemp) {
					displayTemp = gameBoard[i + j * board_width - c * board_height + c].display;
				}
				else if (displayTemp != " " && gameBoard[i + j * board_width - c * board_height + c].display != " " && gameBoard[i + j * board_width - c * board_height + c].display != displayTemp) {
					found = false;
				}
			}
			if (found) {
				return true;
			}
		}
	}
	return false;
}

// alternate turns between two players
int GomokuGame::turn()
{
	if (whoseTurn == "W") {
		whoseTurn = "B";
	}
	else {
		whoseTurn = "W";
	}

	cout << whoseTurn << "'s turn" << endl;
	while (true) {
		unsigned x;
		unsigned y;
		int msg = prompt(x, y);
		if (msg == quit) {
			return quit;
		}
		else if (msg == success) {
			if (x > 0 && x < board_width - 1 && y > 0 && y < board_height - 1 && gameBoard[x + y * board_width].color == no_color) {
				// setting the new move
				gameBoard[x + y * board_width].color = black;
				gameBoard[x + y * board_width].name = "move";
				gameBoard[x + y * board_width].display = whoseTurn;
				(whoseTurn == "W" ? moves2 : moves1).push_back(x);
				(whoseTurn == "W" ? moves2 : moves1).push_back(y);
				--remainingTurns;

				longestDisplayStringLength = max(gameBoard[x + y * board_width].display.length(), longestDisplayStringLength);

				cout << *this << endl;
				cout << endl;

				// printing previous moves
				cout << "Player " << whoseTurn << ": ";
				for (unsigned i = 0; i < (whoseTurn == "W" ? moves2.size() : moves1.size()); i = i + 2) {
					cout << (whoseTurn == "W" ? moves2 : moves1)[i] << "," << (whoseTurn == "W" ? moves2 : moves1)[i + 1] << ";";
				}
				cout << endl;

				return success;
			}
			else {
				continue;
			}
		}
	}
}

// Check whether the player wants to save the game
//  If so, save the current state of the game into a file
//  If not, overwrites the game file with information indicating that
//    next time the game should be start at the beginning
void GomokuGame::saveGame() {
	while (true) {
		cout << "Do you want to save current game? [yes/no]" << endl;
		string input;
		cin >> input;
		if (input == "yes") {
			ofstream ofs;
			ofs.open("Gomoku.txt");
			if (ofs.is_open()) {
				ofs << "Gomoku" << endl;
				ofs << GomokuGame::board_height << endl;
				ofs << GomokuGame::goal << endl;
				ofs << (GomokuGame::whoseTurn == "W" ? "B":"W") << endl;
				ofs << GomokuGame::remainingTurns << endl;
				for (unsigned i = 0; i < GomokuGame::moves1.size(); ++i) {
					ofs << GomokuGame::moves1[i] << " ";
				}
				ofs << endl;
				for (unsigned i = 0; i < GomokuGame::moves2.size(); ++i) {
					ofs << GomokuGame::moves2[i] << " ";
				}
				ofs << endl;
				ofs << GomokuGame::longestDisplayStringLength << endl;
				
				for (unsigned i = 1; i < GomokuGame::board_height - 1; ++i) {
					for (unsigned j = 1; j < GomokuGame::board_width - 1; ++j) {
						if (GomokuGame::gameBoard[GomokuGame::board_width * i + j].display == " ") {
							ofs << "BLANK" << " ";
						}
						else {
							ofs << GomokuGame::gameBoard[GomokuGame::board_width * i + j].display << " ";
						}
					}
				}
				ofs << endl;

				ofs.close();
			}
			return;
		}
		else if (input == "no") {
			overWriteGameFile();
			return;
		}
	}
}

// Overwrite the file with information indicating that next time the game should be start at the beginning
void GomokuGame::overWriteGameFile() {
	ofstream ofs;
	ofs.open("Gomoku.txt");
	if (ofs.is_open()) {
		ofs << "Gomoku" << endl;
		ofs << "NO DATA" << endl;
		ofs.close();
	}
}