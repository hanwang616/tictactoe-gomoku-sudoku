// TicTacToeGame.cpp 
// Han Wang; You Chen
// wang.han@wustl.edu; yoyo.chen@wustl.edu
// File summary:  function definitions for derived class TicTacToeGame
//				Has a constructor which initializes the game board
//				Defines an operator for printing out the game board
//				can decide on whether the game is done
//				can decide on whether the game reachs stalement by checking remaining moves
//				can alternate turns between two players
//				can save the game state


#include "stdafx.h"
#include "GameBase.h"

// defines an operator for printing out the current state of the game board
ostream & operator<< (ostream & o, const TicTacToeGame &game) {
	for (int i = game.board_height - 1; i >= 0; --i) {
		if (i == 0 || i == game.board_height - 1) {
			o << " " << setw(game.longestDisplayStringLength + 1);
		}
		else {
			o << i << setw(game.longestDisplayStringLength + 1);
		}
		for (int j = 0; j < static_cast<int>(game.board_width); ++j) {
			o << game.gameBoard[game.board_width * i + j].display << setw(game.longestDisplayStringLength + 1);
		}
		o << endl;
	}
	o << " " << setw(game.longestDisplayStringLength + 1);
	for (int i = 0; i < static_cast<int>(game.board_width); ++i) {
		if (i == 0 || i == game.board_height - 1) {
			o << " " << setw(game.longestDisplayStringLength + 1);
		}
		else {
			o << i << setw(game.longestDisplayStringLength + 1);
		}
	}
	o << endl;

	return o;
}


// Check whether the file contains a valid previous saved game version
//  If so, initialize the game according to the file
//  If not, initialize the game as it is firstly started up
bool TicTacToeGame::readFromFile() {
	ifstream ifs;
	ifs.open("TicTacToe.txt");
	if (ifs.is_open()) {
		string s;

		//game name
		if (getline(ifs, s)) {
			if (s == "TicTacToe") {

				//gameboard dimensions
				if (getline(ifs, s)) {
					if (s == "NO DATA") {
						return false;
					}
					istringstream issDim(s);
					int dimension = -1;
					if (issDim >> dimension && dimension == 5) {
						board_width = 5;
						board_height = 5;

						//whoseTurn
						if (getline(ifs, s) && (s == "X" || s == "O")) {
							whoseTurn = s;

							//remainingTurns
							if (getline(ifs, s)) {
								istringstream issRT(s);
								int rt = -1;
								if (issRT >> rt && rt > 0) {
									remainingTurns = rt;

									//moves1
									if (getline(ifs, s)) {
										istringstream issM1(s);
										unsigned e1;
										while (issM1 >> e1) {
											TicTacToeGame::moves1.push_back(e1);
										}

										//moves2
										if (getline(ifs, s)) {
											istringstream issM2(s);
											unsigned e2;
											while (issM2 >> e2) {
												TicTacToeGame::moves2.push_back(e2);
											}

											//longestDisplayStringLength
											if (getline(ifs, s)) {
												istringstream issL(s);
												size_t l;
												if (issL >> l) {
													longestDisplayStringLength = l;

													//reading game pieces
													if (getline(ifs, s)) {
														istringstream issPcs(s);
														for (unsigned i = 0; i < board_width*board_height; i++) {
															if (i < board_width || i > board_width*(board_height - 1) - 1 || i % board_width == 0 || i % board_width == board_width - 1) {
																gameBoard.push_back(game_piece(border, "", " "));
															}
															else {
																string p;
																if (issPcs >> p) {
																	if (p == "BLANK") {
																		gameBoard.push_back(game_piece(no_color, "", " "));
																	}
																	else {
																		gameBoard.push_back(game_piece(no_color, "", p));
																	}
																}
																else {
																	return false;
																}
															}
														}
														return true;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	return false;
}

// initialize the TicTacToe game
TicTacToeGame::TicTacToeGame()
{
	if (!readFromFile()) {
		board_width = default_ttt_board_width;
		board_height = default_ttt_board_height;
		whoseTurn = "X";
		remainingTurns = (board_width - 2) * (board_height - 2);
		longestDisplayStringLength = 1;
		for (unsigned i = 0; i < board_width*board_height; i++) {
			if (i < board_width || i > board_width*(board_height - 1) - 1 || i % board_width == 0 || i % board_width == board_width - 1) {
				gameBoard.push_back(game_piece(border, "", " "));
			}
			else {
				gameBoard.push_back(game_piece(no_color, "", " "));
			}
		}
	}
}

// printing out the game board
void TicTacToeGame::print() {
	cout << *this;
}


// decide on whether the someone wins the game
bool TicTacToeGame::done()
{
	// check left to right
	for (unsigned i = 1; i < board_width - 3; ++i) {
		for (unsigned j = 1; j < board_height - 1; ++j) {
			bool found = true;
			for (unsigned c = 1; c < 3; ++c) {
				if (gameBoard[i + j * board_width].display == " " || gameBoard[i + j * board_width].display != gameBoard[i + j * board_width + c].display) {
					found = false;
				}
			}
			if (found) {
				return true;
			}
		}
	}

	// check bottom to top
	for (unsigned i = 1; i < board_width - 1; i++) {
		for (unsigned j = 1; j < board_height - 3; ++j) {
			bool found = true;
			for (unsigned c = 1; c < 3; ++c) {
				if (gameBoard[i + j * board_width].display == " " || gameBoard[i + j * board_width].display != gameBoard[i + j * board_width + c * board_height].display) {
					found = false;
				}
			}
			if (found) {
				return true;
			}
		}
	}

	// check bottom-left to top-right
	for (unsigned i = 1; i < board_width - 3; ++i) {
		for (unsigned j = 1; j < board_height - 3; ++j) {
			bool found = true;
			for (unsigned c = 1; c < 3; ++c) {
				if (gameBoard[i + j * board_width].display == " " || gameBoard[i + j * board_width].display != gameBoard[i + j * board_width + c * board_height + c].display) {
					found = false;
				}
			}
			if (found) {
				return true;
			}
		}
	}

	// check top-left to bottom-right
	for (unsigned i = 1; i < board_width - 3; ++i) {
		for (unsigned j = 3; j < board_height - 1; ++j) {
			bool found = true;
			for (unsigned c = 1; c < 3; ++c) {
				if (gameBoard[i + j * board_width].display == " " || gameBoard[i + j * board_width].display != gameBoard[i + j * board_width - c * board_height + c].display) {
					found = false;
				}
			}
			if (found) {
				return true;
			}
		}
	}

	return false;
}


// decide on whether the game reachs a stalement situation
bool TicTacToeGame::stalement()
{
	if (done() || movesRemain()) {
		return false;
	}
	else {
		return true;
	}
}

// check whether there are still remaining steps which possibly leads to winning
bool TicTacToeGame::movesRemain() {
	// check left to right
	for (unsigned i = 1; i < board_width - 3; ++i) {
		for (unsigned j = 1; j < board_height - 1; ++j) {
			bool found = true;
			string displayTemp = " ";
			for (unsigned c = 0; c < 3; ++c) {
				if (displayTemp == " " && gameBoard[i + j * board_width + c].display != " ") {
					displayTemp = gameBoard[i + j * board_width + c].display;
				}
				else if (displayTemp != " " && gameBoard[i + j * board_width + c].display != " " && gameBoard[i + j * board_width + c].display != displayTemp) {
					found = false;
				}
			}
			if (found) {
				return true;
			}
		}
	}

	// check bottom to top
	for (unsigned i = 1; i < board_width - 1; i++) {
		for (unsigned j = 1; j < board_height - 3; ++j) {
			bool found = true;
			string displayTemp = " ";
			for (unsigned c = 0; c < 3; ++c) {
				if (displayTemp == " " && gameBoard[i + j * board_width + c * board_height].display != " ") {
					displayTemp = gameBoard[i + j * board_width + c * board_height].display;
				}
				else if (displayTemp != " " && gameBoard[i + j * board_width + c * board_height].display != " " && gameBoard[i + j * board_width + c * board_height].display != displayTemp) {
					found = false;
				}
			}
			if (found) {
				return true;
			}
		}
	}

	// check bottom-left to top-right
	for (unsigned i = 1; i < board_width - 3; ++i) {
		for (unsigned j = 1; j < board_height - 3; ++j) {
			bool found = true;
			string displayTemp = " ";
			for (unsigned c = 0; c < 3; ++c) {
				if (displayTemp == " " && gameBoard[i + j * board_width + c * board_height + c].display != " ") {
					displayTemp = gameBoard[i + j * board_width + c * board_height + c].display;
				}
				else if (displayTemp != " " && gameBoard[i + j * board_width + c * board_height + c].display != " " && gameBoard[i + j * board_width + c * board_height + c].display != displayTemp) {
					found = false;
				}
			}
			if (found) {
				return true;
			}
		}
	}

	// check top-left to bottom-right
	for (unsigned i = 1; i < board_width - 3; ++i) {
		for (unsigned j = 3; j < board_height - 1; ++j) {
			bool found = true;
			string displayTemp = " ";
			for (unsigned c = 0; c < 3; ++c) {
				if (displayTemp == " " && gameBoard[i + j * board_width - c * board_height + c].display != " " && gameBoard[i + j * board_width - c * board_height + c].display != displayTemp) {
					displayTemp = gameBoard[i + j * board_width - c * board_height + c].display;
				}
				else if (displayTemp != " " && gameBoard[i + j * board_width - c * board_height + c].display != " " && gameBoard[i + j * board_width - c * board_height + c].display != displayTemp) {
					found = false;
				}
			}
			if (found) {
				return true;
			}
		}
	}
	return false;
}


// alternate turns between two players
int TicTacToeGame::turn()
{
	if (whoseTurn == "X") {
		whoseTurn = "O";
	}
	else {
		whoseTurn = "X";
	}

	cout << whoseTurn << "'s turn" << endl;
	while (true) {
		unsigned x;
		unsigned y;
		int msg = prompt(x, y);
		if (msg == quit) {
			return quit;
		}
		else if (msg == success) {
			if (x > 0 && x < board_width - 1 && y > 0 && y < board_height - 1 && gameBoard[x + y * board_width].color == no_color) {
				// setting the new move
				gameBoard[x + y * board_width].color = black;
				gameBoard[x + y * board_width].name = "move";
				gameBoard[x + y * board_width].display = whoseTurn;
				(whoseTurn == "X" ? moves2 : moves1).push_back(x);
				(whoseTurn == "X" ? moves2 : moves1).push_back(y);
				--remainingTurns;

				longestDisplayStringLength = max(gameBoard[x + y * board_width].display.length(), longestDisplayStringLength);

				cout << *this << endl;
				cout << endl;

				// printing previous moves
				cout << "Player " << whoseTurn << ": ";
				for (unsigned i = 0; i < (whoseTurn == "X" ? moves2.size() : moves1.size()); i = i + 2) {
					cout << (whoseTurn == "X" ? moves2 : moves1)[i] << "," << (whoseTurn == "X" ? moves2 : moves1)[i + 1] << ";";
				}
				cout << endl;

				return success;
			}
			else {
				continue;
			}
		}
	}
}


// Check whether the player wants to save the game
//  If so, save the current state of the game into a file
//  If not, overwrites the game file with information indicating that
//    next time the game should be start at the beginning
void TicTacToeGame::saveGame() {
	while (true) {
		cout << "Do you want to save current game? [yes/no]" << endl;
		string input;
		cin >> input;
		if (input == "yes") {
			ofstream ofs;
			ofs.open("TicTacToe.txt");
			if (ofs.is_open()) {
				ofs << "TicTacToe" << endl;
				ofs << TicTacToeGame::board_height << endl;
				ofs << (TicTacToeGame::whoseTurn == "X" ? "O":"X") << endl;
				ofs << TicTacToeGame::remainingTurns << endl;
				for (unsigned i = 0; i < TicTacToeGame::moves1.size(); ++i) {
					ofs << TicTacToeGame::moves1[i] << " ";
				}
				ofs << endl;
				for (unsigned i = 0; i < TicTacToeGame::moves2.size(); ++i) {
					ofs << TicTacToeGame::moves2[i] << " ";
				}
				ofs << endl;
				ofs << TicTacToeGame::longestDisplayStringLength << endl;

				for (unsigned i = 1; i < TicTacToeGame::board_height - 1; ++i) {
					for (unsigned j = 1; j < TicTacToeGame::board_width - 1; ++j) {
						if (TicTacToeGame::gameBoard[TicTacToeGame::board_width * i + j].display == " ") {
							ofs << "BLANK" << " ";
						}
						else {
							ofs << TicTacToeGame::gameBoard[TicTacToeGame::board_width * i + j].display << " ";
						}
					}
				}
				ofs << endl;

				ofs.close();
			}
			return;
		}
		else if (input == "no") {
			overWriteGameFile();
			return;
		}
	}
}


// Overwrite the file with information indicating that next time the game should be start at the beginning
void TicTacToeGame::overWriteGameFile() {
	ofstream ofs;
	ofs.open("TicTacToe.txt");
	if (ofs.is_open()) {
		ofs << "TicTacToe" << endl;
		ofs << "NO DATA" << endl;
		ofs.close();
	}
}