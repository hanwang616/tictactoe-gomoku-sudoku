// GamePiece.cpp 
// Han Wang; You Chen
// wang.han@wustl.edu; yoyo.chen@wustl.edu
// File summary:  Specification of a game_piece, containing color, name and display name.
//					Implementation for game_piece, constructor

#include "stdafx.h"
#include "GamePiece.h"

game_piece::game_piece(piece_color c, string n, string d)
	:color(c), name(n), display(d){};

